About
--------
Project Name:  CST-235_Week1_Assignment1
Version:  1.0
Created:  11/8/2017
Last Modified:  11/9/2017

Created by Nicholas Robertson
CST-235 - Grand Canyon University

Classes
---------
1)  VideoFileSize.java

This is the only bean in the project.  It follows the requirements of a bean, namely:
- Implements the Serializable interface
- Properties are private, with public getters and setters
- Includes a no-argument constructor

The two mutable properties are "quality" and "seconds".  When instantiating this class, the user must set a value for quality and seconds.
- "quality" is a public enum (called VideoQuality) with two options, HD720p and HD1080p.
- "seconds" is an integer and must be >= 0
- Both properties have getters and setters.  Naming convention is getX and setX, where X is the name of the property.
	- Example:  getQuality(), getSeconds(), setQuality(), setSeconds()

The two public methods are calculateTotalMB() and calculateTotalGB().  These should be called after the two properties listed above are properly set.
- calculateTotalMB() finds the estimated file size of a video, taking a fixed bitrate based on the selected quality and multiplying it by the duration.  This number is then divided by 8 to conver the megabites to megabytes.  The method returns a double value to the user.
- calculateTotalGB() simply calls calculateTotalMB() and divides its value by 1000 to convert from megabytes to gigabytes.  This method also returns a double value.

2)  Main.java

This is the main executable and is executed in a console.  There are two objects instantiated:
- scanner:  Used to read String values in the console
- file:  Instantiation of the VideoFileSize bean.  Default property values are set, but will be changed later based on user input.

Execution moves to a while loop, waiting for the user to select either option 1 (720p) or option 2 (1080p).  Checks are run to ensure an integer value is entered.
- If not an integer, a message is displayed to the user indicating an invalid input.
- If a valid integer, switch is called to evaluate the input.  If a 1 or 2 is input, the file.setQuality() method is called, setting the appropriate video quality.  Otherwise, a message is displayed to the user indicating an invalid selection.

Execution moves to the next while loop, this time asking for the video's duration in seconds.  The same check is used as with quality, ensuring an integer is entered.  Non-integers display a message to the user, as well as integers below 0.  Once a valid input has been accepted, file.setSeconds() is called and sets the proper duration.

Now that the quality and seconds properties are set, the last step is to call the calculation methods.  The results are displayed to the console, showing the file size in both megabytes and gigabytes.