public class Product
{
	// Some properties are publicly accessible
	// Not recommended, but allowable since this is not a JavaBean class 
	public String name;
	public String description;
	private double price;
	
	// Lack of a no-arg constructor is allowed
	public Product(String name, String description, double price)
	{
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	// Getters and setters not required to follow a naming convention (though it is recommended)
	// Getter
	public double price()
	{
		return price;
	}
	
	// Setter
	public void price(double price)
	{
		this.price = price;
	}
}