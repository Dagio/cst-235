import java.io.Serializable;

public class ProductJavaBean implements Serializable
{
	private String name;
	private String description;
	private double price;
	
	// No-arg constructor
	public ProductJavaBean()
	{
		name = "N/A";
		description = "N/A";
		price = 0.0;
	}
	
	public ProductJavaBean(String name, String description, double price)
	{
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	// Getters
	public String getName()
	{
		return name;
	}
	public String getDescription()
	{
		return description;
	}
	public double getPrice()
	{
		return price;
	}
	
	// Setters
	public void setName(String name)
	{
		this.name = name;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public void setPrice(double price)
	{
		this.price = price;
	}
}