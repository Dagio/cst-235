/*
 * Project Name:  CST-235_Week1_Assignment2
 * Version:  1.0
 * Created:  11/11/2017
 * Last Modified:  11/12/2017
 * 
 * Created by Nicholas Robertson
 * CST-235 - Grand Canyon University
*/

public class Main
{
	public static void main(String[] args)
	{
		// Entity bean
		/*
		EntityTxtReader entityTxtReader = new EntityTxtReader();
		entityTxtReader.SaveState("TxtReader.xml");
		entityTxtReader = entityTxtReader.LoadState("TxtReader.xml");
		
		// Display results from entity bean
		System.out.println("Reading file \"Text.txt\"");
		System.out.println("Total characters: " + entityTxtReader.getTotalCharacters());
		System.out.println("Total words: " + entityTxtReader.getTotalWords());
		System.out.println("Sum of numbers: " + entityTxtReader.getSumOfNumbers());
		*/
		
		// Entity bean implementation is commented out, but code runs fine.
		// Session bean is the implementation for the purpose of this assignment. 
		
		// Session bean
		SessionTxtReader sessionTxtReader = new SessionTxtReader();
		
		// Display results from session bean
		System.out.println("Reading file \"Text.txt\"");
		System.out.println("Total characters: " + sessionTxtReader.getTotalCharacters());
		System.out.println("Total words: " + sessionTxtReader.getTotalWords());
		System.out.println("Sum of numbers: " + sessionTxtReader.getSumOfNumbers());
	}
}
