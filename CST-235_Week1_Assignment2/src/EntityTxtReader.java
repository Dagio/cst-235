import java.beans.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class EntityTxtReader implements Serializable
{
	private int totalCharacters;
	private int totalWords;
	private int sumOfNumbers;
	private String text;
	
	public EntityTxtReader()
	{
		setTotalCharacters(0);
		setTotalWords(0);
		setSumOfNumbers(0);
		setText("");
	}
	
	public EntityTxtReader(String filePath)
	{
		ReadFile(filePath);
	}
	
	public void ReadFile(String filePath)
	{
		try
		{
			byte[] bytes = Files.readAllBytes(Paths.get(filePath));
			// "Cp1252" is ANSI encoding
			// Reference:  https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html
			// Text file must be encoded in ANSI (aka Windows-1252)
			text = new String(bytes, "Cp1252");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		calcTotalCharacters();
		calcTotalWords();
		calcSumOfNumbers();
	}
	
	// Saves the class property values to the .xml file specified
	public void SaveState(String filePath)
	{
		try
		{
			XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(filePath)));
			encoder.writeObject(this);
			encoder.close();
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	/* 
	 * Returns an EntityTxtReader object from the specified .xml file
	 * 
	 * Proper use would be:
	 * myTextReader = myTextReader.LoadState("myFile.xml");
	 */
	public EntityTxtReader LoadState(String filePath)
	{
		try
		{
			XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(filePath)));
			EntityTxtReader reader = (EntityTxtReader)decoder.readObject();
			decoder.close();
			return reader;
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
		return null;
	}

	// Private setters - used to calculate after reading the .txt file
	private void calcTotalCharacters()
	{
		this.totalCharacters = text.length();
	}
	private void calcTotalWords()
	{
		/* 
		 * \\W+ splits at each new word
		 * Create an array with each element a new word
		 * Length of the array is the total number of words
		 */
		String[] words = text.split("\\W+");
		this.totalWords = words.length;
	}
	private void calcSumOfNumbers()
	{	
		// Default sum to 0, then add to total as loop iterates
		this.sumOfNumbers = 0;
		
		for (int i = 0; i < text.length(); i++)
		{
			if (Character.isDigit(text.charAt(i)))
			{
				this.sumOfNumbers += Character.getNumericValue(text.charAt(i));
			}
		}
	}
	
	// Getters
	public int getTotalCharacters()
	{
		return totalCharacters;
	}
	public int getTotalWords()
	{
		return totalWords;
	}
	public int getSumOfNumbers()
	{
		return sumOfNumbers;
	}
	public String getText()
	{
		return text;
	}
	
	// Setters - these must be public or the XML Encoder will not save these values
	public void setTotalCharacters(int totalCharacters)
	{
		this.totalCharacters = totalCharacters;
	}
	public void setTotalWords(int totalWords)
	{
		this.totalWords = totalWords;
	}
	public void setSumOfNumbers(int sumOfNumbers)
	{
		this.sumOfNumbers = sumOfNumbers;
	}
	public void setText(String text)
	{
		this.text = text;
	}
}