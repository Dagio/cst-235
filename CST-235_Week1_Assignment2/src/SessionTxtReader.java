import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SessionTxtReader
{
	private int totalCharacters;
	private int totalWords;
	private int sumOfNumbers;
	private String text;

	public SessionTxtReader()
	{
		try
		{
			byte[] bytes = Files.readAllBytes(Paths.get("Text.txt"));
			// "Cp1252" is ANSI encoding
			// Reference:  https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html
			// Text file must be encoded in ANSI (aka Windows-1252)
			text = new String(bytes, "Cp1252");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setTotalCharacters();
		setTotalWords();
		setSumOfNumbers();
	}

	// Private setters - used to calculate after reading the .txt file
	private void setTotalCharacters()
	{
		totalCharacters = text.length();
	}
	private void setTotalWords()
	{
		/* 
		 * \\W+ splits at each new word
		 * Create an array with each element a new word
		 * Length of the array is the total number of words
		 */
		String[] words = text.split("\\W+");
		totalWords = words.length;
	}
	private void setSumOfNumbers()
	{	
		// Default sum to 0, then add to total as loop iterates
		sumOfNumbers = 0;
		
		for (int i = 0; i < text.length(); i++)
		{
			if (Character.isDigit(text.charAt(i)))
			{
				sumOfNumbers += Character.getNumericValue(text.charAt(i));
			}
		}
	}
	
	// Getters
	public int getTotalCharacters()
	{
		return totalCharacters;
	}
	public int getTotalWords()
	{
		return totalWords;
	}
	public int getSumOfNumbers()
	{
		return sumOfNumbers;
	}
}