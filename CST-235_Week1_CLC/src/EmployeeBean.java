import java.io.Serializable;

public class EmployeeBean implements Serializable
{
	private String firstName;
	private String lastName;
	private double salary;
	private boolean isFullTime;
	
	public EmployeeBean()
	{
		setFirstName("N/A");
		setLastName("N/A");
		setSalary(0);
		setIsFullTime(true);
	}
	
	// Getters
	public String getFirstName()
	{
		return firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public double getSalary()
	{
		return salary;
	}
	public boolean getIsFullTime()
	{
		return isFullTime;
	}

	// Setters
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	public void setSalary(double salary)
	{
		this.salary = salary;
	}
	public void setIsFullTime(boolean isFullTime)
	{
		this.isFullTime = isFullTime;
	}
}
