/*
 * Project Name:  CST-235_Week1_CLC
 * Version:  1.0
 * Created:  11/12/2017
 * Last Modified:  11/12/2017
 * 
 * Created by Nicholas Robertson
 * CST-235 - Grand Canyon University
*/

import java.beans.*;
import java.io.*;

public class Main
{

	public static void main(String[] args)
	{
		// Instantiate bean and set property values
		EmployeeBean bean = new EmployeeBean();
		bean.setFirstName("John");
		bean.setLastName("Smith");
		bean.setIsFullTime(true);
		bean.setSalary(45000);
		
		// Question 1 Code Example
		// Use XML Encoder to save bean's values into an .xml file
		try
		{
			XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("Employee.xml")));
			encoder.writeObject(bean);
			encoder.close();
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
		// Use XML Decoder to load bean's values from an .xml file
		try
		{
			XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("Employee.xml")));
			EmployeeBean employee = (EmployeeBean)decoder.readObject();
			decoder.close();
			
			// Display loaded bean's values
			System.out.println("Question 1 Example");
			System.out.println("First name - " + employee.getFirstName());
			System.out.println("Last name - " + employee.getLastName());
			System.out.println("Salary - " + employee.getSalary());
			System.out.println("Full time - " + employee.getIsFullTime() + "\r\n");
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
		// Question 2 Code Example
		try
		{
			// Display bean's properties
			System.out.println("Question 2 Example");
			System.out.println("Properties for EmployeeBean.class:\r\n");
			BeanInfo beanInfo = Introspector.getBeanInfo(EmployeeBean.class);		
			for (PropertyDescriptor property : beanInfo.getPropertyDescriptors())
			{
				System.out.println("Name - " + property.getName());
				System.out.println("Type - " + property.getPropertyType());
				System.out.println("Read Method - " + property.getReadMethod());
				System.out.println("Write Method - " + property.getWriteMethod() + "\r\n");
			}
		}
		catch (IntrospectionException ex)
		{
			System.out.println(ex.getMessage());
		}	
	}
}
